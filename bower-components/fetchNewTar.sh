#!/bin/bash
rm -rf bower_components bower-components-*-build.tar.gz
bower install

rm -rf bower_components/font-roboto
cp -r font-roboto bower_components

rm -rf bower_components/iron-ajax/iron-ajax.html
cp iron-ajax/iron-ajax.html bower_components/iron-ajax

rm -rf bower_components/vaadin-grid/vaadin-grid.html
cp vaadin-grid/vaadin-grid.html bower_components/vaadin-grid

rm -rf bower_components/saveSvgAsPng/saveSvgAsPng.js
cp saveSvgAsPng/saveSvgAsPng.js bower_components/saveSvgAsPng

cp index.html bower_components
tar -czvf bower-components-1.0.5-build.tar.gz bower_components
rm -rf bower_components
