#
# spec file for PugiXML
#
AutoReqProv: no
Name: %{name}
Version: %{version}
Release: %{release}
Packager: %{packager}
Summary: Bower components (Polymer, jQuery, ...) for the Trigger Supervisor
License: MIT
Group: trigger
Source: %{tar_file}
URL:  https://svnweb.cern.ch/trac/cactus/browser/trunk/cactuscore/extern/bower-components
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix: %{_prefix}

%description
Bower components (Polymer, jQuery, ...) for the Trigger Supervisor


%prep

%build


%install

# copy includes to RPM_BUILD_ROOT and set aliases
mkdir -p $RPM_BUILD_ROOT%{_prefix}/htdocs
cp -rp %{sources_dir}/htdocs/* $RPM_BUILD_ROOT%{_prefix}/htdocs/.

#Change access rights
chmod -R 755 $RPM_BUILD_ROOT%{_prefix}/htdocs

%clean

%post

%postun

%files
%defattr(-, root, root)
%{_prefix}/htdocs/*
