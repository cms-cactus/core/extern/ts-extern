#
# spec file for PugiXML
#
AutoReqProv: no
Name: %{name}
Version: %{version}
Release: %{release}
Packager: %{packager}
Summary: Node modules for the Trigger Supervisor
License: MIT
Group: trigger
Source: %{tar_file}
URL:  https://svnweb.cern.ch/trac/cactus/browser/trunk/cactuscore/extern/node-modules
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix: %{_prefix}

%description
Node modules library for the Trigger supervisor
Installs to /opt/xdaq/lib/node_modules
To give a central location for developers


%prep

%build


%install

# copy includes to RPM_BUILD_ROOT and set aliases
mkdir -p $RPM_BUILD_ROOT%{_prefix}
cp -rp %{sources_dir}/* $RPM_BUILD_ROOT%{_prefix}/.

#Change access rights
chmod -R 755 $RPM_BUILD_ROOT%{_prefix}

%clean

%post

%postun

%files
%defattr(-, root, root)
%{_prefix}/*
